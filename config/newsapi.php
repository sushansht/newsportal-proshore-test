<?php

return [

    /*
    |--------------------------------------------------------------------------
    | News Api Configurations
    |--------------------------------------------------------------------------
    |
    */

    'url' => env('NEWSAPI_URL', 'https://newsapi.org'),

    'key' => env('NEWSAPI_KEY' , '80f4c96a1d6c4dd5b54689ab319a1475')
];
