<?php

use App\Http\Controllers\News\NewsController;
use App\Http\Controllers\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


$UserApiGroup = [
    'prefix' => 'v1',
    'as' => 'api::',
];

Route::group($UserApiGroup , function () {
    Route::post('signIn' , [UserController::class, 'signIn']);
    Route::post('signUp' , [UserController::class, 'store']);
});

Route::group($UserApiGroup + ['middleware => auth.api'] , function () {
    Route::get('signOut' ,  [UserController::class, 'signOut']);
    Route::post('token/refresh' , [UserController::class, 'refreshToken']);
});


$NewsApiGroup = [
    'prefix' => 'v1',
    'as' => 'api::',
];

Route::group($NewsApiGroup , function () {
    Route::get('list' , [NewsController::class, 'getNewsList']);
    Route::post('react' , [NewsController::class, 'postReaction']);
    Route::get('sources' , [NewsController::class, 'getSources']);
});

