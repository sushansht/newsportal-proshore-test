<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('user_news', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')
                        ->constrained('users')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
            $table->text('news_url');
            $table->boolean('like_state')->nullable(); //null of no reaction , 0 for dislike , 1 for like
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_news');
    }
}
