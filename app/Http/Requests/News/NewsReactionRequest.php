<?php

namespace App\Http\Requests\News;

use Illuminate\Foundation\Http\FormRequest;

class NewsReactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'news_url' => 'required | url',
            'like_state' => 'required | boolean'
        ];
    }


    /**
     * return json response
     *
     * @param  mixed $errors
     * @return void
     */
    public function response(array $errors)
    {
      return response()->json($errors);
    }
}
