<?php

namespace App\Http\Repositories;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Log;

class Repository
{
	/**
	 * Error Message.
	 *
	 * @var string
	 */
	private $errorMessage;

	/**
	 * Status Code
	 *
	 * @var integer
	 */
	private $statusCode;

	/**
	 * Return Error Message
	 *
	 * @return string
	 */
	public function getErrorMessage()
	{
		return $this->errorMessage;
	}

	/**
	 * Return Error Message
	 *
	 * @return string
	 */
	public function getStatusCode()
	{
		return $this->statusCode;
	}


	/**
	 * Set Error Message
	 *
	 * @param string $message
	 */
	protected function setError($message, $code)
	{
		$this->errorMessage = $message;
		$this->statusCode = $code;
		return false;
	}


    /**
     * function to fetch data from given url
     *
     * @param  mixed $url
     * @return void
     */
    public function getData($url)
    {
        try {
            $headers = ['Accept' => 'application/json', ];

            $client = new Client();
            $response = $client->request('GET' , $url , $headers );

            $response = $response->getBody();

            return json_decode($response);
        } catch (\Exception $e) {
            Log::error("cannot fetch data : ".$e->getMessage());
            return $this->setError('Cannot fetch data' , 500);
        }catch (ClientException $e){
            Log::error("cannot fetch data : ".$e->getResponse()->getBody(true));
            return $this->setError('Cannot fetch data ', 500);
         }
    }
}
