<?php

namespace App\Http\Repositories\News;

use App\Http\Repositories\Repository;


class NewsRepository extends Repository
{

    /**
     * url of newsapi
     *
     * @var mixed
     */
    private $url;

    /**
     * authentication key of newsapi
     *
     * @var mixed
     */
    private $key;


    /**
     * __construct
     *
     * @return void
     */
    public function __construct( )
    {
        $this->url = config('newsapi.url');
        $this->key = config('newsapi.key');
    }


    /**
     * get source list of newsapi
     *
     * @return void
     */
    public function getSources()
    {
        return $this->getData($this->url . '/v2/sources?apiKey='.$this->key)->sources;
    }

    /**
     * fetch top headlines from newsapi
     *
     * @param  mixed $query
     * @return void
     */
    public function getTopHeadlines($query = [])
    {

        $q = implode('&', array_map(
            function ($v, $k) {return sprintf("%s=%s", $k, $v);},
            $query,
            array_keys($query)
        ));

        return $this->getData($this->url . '/v2/top-headlines?' . $q . '&apiKey=' . $this->key);
    }

}
