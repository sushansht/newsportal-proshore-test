<?php

namespace App\Http\Repositories\User;

use App\Http\Repositories\Repository;
use App\Models\User;

class UserRepository extends Repository
{
    /**
     * user model
     *
     * @var mixed
     */
    private $model;

    /**
     * __construct
     *
     * @param  mixed $model
     * @return void
     */
    public function __construct( User $model)
    {
        $this->model = $model;
    }

    /**
     * get user by email
     *
     * @param  mixed $email
     * @return void
     */
    public function getByEmail($email)
    {
        return $this->model->where('email' , $email)->first();
    }

    /**
     * create new User by data
     *
     * @param  mixed $data
     * @return object
     */
    public function createUser($data)
    {
        return $this->model->create($data);
    }
}
