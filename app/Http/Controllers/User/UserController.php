<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Repositories\User\UserRepository;
use App\Http\Requests\User\UserRequest;
use App\Http\Services\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    /**
     * user service for DI
     *
     * @var mixed
     */
    private $userService;

    /**
     * user Repository for DI
     *
     * @var mixed
     */
    private $userRepo;


    /**
     * __construct with Dependency Injection
     *
     * @return void
     */
    public function __construct( UserService $userService , UserRepository $userRepo )
    {
        $this->userService = $userService;
        $this->userRepo = $userRepo;
    }


    /**
     * signup user
     *
     * @param  mixed $request
     * @return void
     */
    public function store(UserRequest $request)
    {
        // dd($request);
        $data = $request->all();

        if(!$user = $this->userService->createUser($data)){
            return response()->json($this->userService->getErrorMessage(),
                                    $this->userService->getStatusCode() );

        }

        return response()->json($user, 200);

    }


    /**
     * sign in user
     *
     * @param  mixed $request
     * @return void
     */
    public function signIn(Request $request)
    {
        $credentials = request(['email', 'password']);

        if(!$user = $this->userRepo->getByEmail($request->email))
            return response()->json([ 'error' => 'Credential doesnot match' ], 401);

        $claims  = ['email' => $user->email];

        if (!$token = JWTAuth::claims($claims)->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = $this->userRepo->getByEmail($request->email);

        return $this->respondWithToken($user , $token);
    }

    /**
     * sign out user
     *
     * @param  mixed $request
     * @return void
     */
    public function signOut(Request $request)
    {
        Auth::logout();

        return response()->json(['message' => 'Successfully Signed out']);
    }

    /**
     * get refresh token
     *
     * @param  mixed $request
     * @return void
     */
    public function refreshToken(Request $request)
    {
        $payload = Auth::payload();

        $email = $payload->get('email');

        if(!$user = $this->userRepo->getByEmail($email))
            return response()->json([ 'error' => 'Unauthorize to get tokenn' ], 401);

        return $this->respondWithToken($user , Auth::refresh());
    }

    /**
     * respondWithToken
     *
     * @param  mixed $token
     * @return void
     */
    protected function respondWithToken( $user, $token)
    {
        return response()->json([
            'user' => $user,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ]);
    }

}
