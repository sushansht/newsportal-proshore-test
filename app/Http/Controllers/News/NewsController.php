<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Http\Requests\News\NewsReactionRequest;
use App\Http\Services\News\NewsService;
use App\Models\UserNews;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    /**
     * DI for NewsService class
     *
     * @var mixed
     */
    private $newsService;

    /**
     * __construct
     *
     * @param  mixed $newsService
     * @return void
     */
    public function __construct( NewsService $newsService )
    {
        $this->newsService = $newsService;
    }

    /**
     * get news list with filter
     *
     * @param  mixed $request
     * @return void
     */
    public function getNewsList(Request $request){
       $data = $this->newsService->getNewsList($request->all());

       return response()->json($data , 200);
    }

    /**
     * post reaction of user
     *
     * @param  mixed $request
     * @return void
     */
    public function postReaction(NewsReactionRequest $request){
            $user_news = UserNews::create([
                'user_id' => 1,
                'news_url' => $request->news_url,
                'like_state' => $request->like_state
            ]);

            if(!$user_news)
                return response()->json(['error' => 'Cannnot register reaction, Please Try Later!' ] , 500);

        return response()->json('reacted succesfully!' , 200);
    }

    /**
     * get available source list
     *
     * @param  mixed $request
     * @return void
     */
    public function getSources(Request $request){

        if(!$sources =   $this->newsService->getSourceList()){
            return response()->json(
                [
                    'error' => $this->newsService->getErrorMessage(),
                ],
                $this->newsService->getStatusCode()
            );
        }

        return response()->json( $sources , 200 );
    }
}
