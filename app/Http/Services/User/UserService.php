<?php

namespace App\Http\Services\User;

use App\Http\Repositories\User\UserRepository;
use App\Http\Services\Service;
use Exception;
use Log;

class UserService extends Service
{
    private $userRepo;

    public function __construct( UserRepository $userRepo )
    {
        $this->userRepo = $userRepo;
    }

    /**
     * create new User by data
     *
     * @param  mixed $data
     * @return void
     */
    public function createUser($data)
    {
        try{
           $user = $this->userRepo->createUser($data);
           Log::info($user->email .' User Created');
           return $user;
        }catch(Exception $e){
            Log::error('Error occur while creating user: '.$e->getMessage());
            return $this->setError('Cannot Create User, Please Try Again!!', 500);
        }
    }
}
