<?php

namespace App\Http\Services;

class Service
{
	/**
	 * Error Message.
	 *
	 * @var string
	 */
	private $errorMessage;

	/**
	 * Status Code
	 *
	 * @var integer
	 */
	private $statusCode;

	/**
	 * Return Error Message
	 *
	 * @return string
	 */
	public function getErrorMessage()
	{
		return $this->errorMessage;
	}

	/**
	 * Return Error Message
	 *
	 * @return string
	 */
	public function getStatusCode()
	{
		return $this->statusCode;
	}

	/**
	 * Set Error Message
	 *
	 * @param string $message
	 */
	protected function setError($message, $code)
	{
		$this->errorMessage = $message;
		$this->statusCode = $code;
		return false;
	}

}
