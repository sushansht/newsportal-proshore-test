<?php

namespace App\Http\Services\News;

use App\Http\Repositories\News\NewsRepository;
use App\Http\Services\Service;
use App\Models\UserNews;
use Exception;
use Log;

class NewsService extends Service
{
    /**
     * DI for news repo
     *
     * @var mixed
     */
    private $newsRepo;

    public function __construct( NewsRepository $newsRepo )
    {
        $this->newsRepo = $newsRepo;
    }

    /**
     * get source list
     *
     * @param  mixed $data
     * @return void
     */
    public function getSourceList($data = [])
    {
        if(!$sources = $this->newsRepo->getSources()){
            return $this->setError(
                $this->newsRepo->getErrorMessage(),
                $this->newsRepo->getStatusCode()
            );
        }

        return $sources;
    }


    /**
     * get news list
     *
     * @param  mixed $data
     * @return void
     */
    public function getNewsList($data = [])
    {
        // if(!){
        //     return $this->setError(
        //         $this->newsRepo->getErrorMessage(),
        //         $this->newsRepo->getStatusCode()
        //     );
        // }
        $topnews = $this->newsRepo->getTopHeadlines($data);
         collect($topnews->articles)->transform(function ($item, $key) {
             $item->like_count = UserNews::where('news_url' , $item->url)->where('like_state' , 1)->get()->count();
             $item->dislike_count = UserNews::where('news_url' , $item->url)->where('like_state' , 0)->get()->count();
             return $item;
        });

        return $topnews;
    }
}
