##Project Setup :
please run following command to get started:  <br />
1. php artisan key:generate  <br />
    It will generate new application key

2. php artisan config:cache <br />
    It will set configuration. newsapi keys are also stored in config file so it is necessary to get news. <br />

3. php artsan migrate <br />
    It will migrate all the table <br />

4. npm run dev <br />
    it will run server for react js.


## About the System :

    The news from newsapi will be fetched by backend for every api request of news to get upto date news. News will nnot be stored in database because it will take might take more processing time and might not be much useful since newsapi will provide all the news <br />

    Like and Dislike of user will be stored in database with unique identifier of newsapi news. since there is not any id or news url will be used to determine the particular news that user has liked.  <br />

    News detail can be viewed by clicking title of the news. like and dislike is available in detail page.<br />

    User authentication with signup and signin is implement by using jwt auth tool. <br />

    News list can be filter by category , country and source which are listed in navigation section of page.<br />

    Free template is being used because it will take much more time to design and develop new template.<br />



