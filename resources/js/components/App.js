import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Common/Header'
import Footer from './Common/Footer'
import NavBar from './Common/NavBar'
import List from './List/List'
import SignUp from './Auth/SignUp'
import SignIn from './Auth/SignIn'
import { AuthProvider } from "./Contexts/AuthContext"
import Detail from './List/Detail'


class App extends Component {

  render () {
    return (
      <BrowserRouter>
        <div>
        <AuthProvider>
        <Header />
        <NavBar/>
            <Switch>
                <Route exact path="/" component={List}  />
                <Route exact path="/category/:categoryName" component={List}  />
                <Route exact path="/source/:sourceName" component={List}  />
                <Route exact path="/country/:countryName" component={List}  />
                <Route exact path="/signup" component={SignUp}  />
                <Route exact path="/signin" component={SignIn}  />
            </Switch>
        </AuthProvider>
        <Footer />
        </div>
      </BrowserRouter>
    )
  }
}

if (document.getElementById('app')) {
ReactDOM.render(<App />, document.getElementById('app'))
}
