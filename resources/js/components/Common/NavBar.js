import axios from 'axios'
import React , {setState , useState, useEffect} from 'react'
import { Link } from 'react-router-dom'
import {useAuth} from '../Contexts/AuthContext'

function NavBar(props) {

    const [ sources , setSources ] = useState([])
    const [ countries, setCountries ] = useState([
        {
            'id' : 'ae',
            'name' : 'UAE'
        },
        {
            'id' : 'au',
            'name' : 'Australia'
        },
        {
            'id' : 'ca',
            'name' : 'Canada'
        }
    ])

    const { signOut , changeCurrentUser} = useAuth()

    useEffect(() => {
        axios({
            method: 'GET',
            url: 'http://localhost:8000/api/v1/sources',

          }).then(res => {

              setSources(res.data)
          })

    }, [setSources])

    function handleSignOut(){

        signOut().then( res => {
            localStorage.removeItem('user_id');
            localStorage.removeItem('access_token')
            changeCurrentUser({})
            history.push('/')
        })
    }


  return (
        <div>
        <header className="header">
            <div className="container">
                <nav className="navbar navbar-inverse navbar-toggleable-md">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#cloapediamenu" aria-controls="cloapediamenu" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse justify-content-md-center" id="cloapediamenu">
                        <ul className="navbar-nav">
                        <li className="nav-item" key="home">
                            <a className="nav-link color-pink-hover" href="/">Home</a>
                            </li>
                            <li className="nav-item" key="business">
                            <Link className="nav-link color-pink-hover" to="/category/business">Business</Link>
                            </li>
                            <li className="nav-item" key="entertainment">
                            <Link className="nav-link color-pink-hover" to="/category/entertainment">Entertainment</Link>
                            </li>
                            <li className="nav-item" key="general">
                            <Link className="nav-link color-pink-hover" to="/category/general">General</Link>

                            </li>
                            <li className="nav-item" key="health">
                            <Link className="nav-link color-pink-hover" to="/category/health">Health</Link>

                            </li>
                            <li className="nav-item" key="science">
                            <Link className="nav-link color-pink-hover" to="/category/science">Science</Link>

                            </li>
                            <li className="nav-item" key="sports">
                            <Link className="nav-link color-pink-hover" to="/category/sports">Sports</Link>

                            </li>
                            <li className="nav-item" key="technology">
                            <Link className="nav-link color-pink-hover" to="/category/technology">Technology</Link>

                            </li>
                            <li className="nav-item dropdown has-submenu">
                                <a className="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sources</a>
                                <ul className="dropdown-menu" aria-labelledby="dropdown05">
                                {sources.map((item , key) => {

                                   return <li key={item.id}>
                                                 <Link className="dropdown-item" to={"/source/"+item.id}>{item.name}</Link>
                                        {/* <a className="dropdown-item" href="#">
                                            {item.name}
                                            </a> */}
                                    </li>
                                })}
                                </ul>
                            </li>

                            <li className="nav-item dropdown has-submenu">
                                <a className="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Contries</a>
                                <ul className="dropdown-menu" aria-labelledby="dropdown05">
                                {countries.map((item , key) => {
                                    return <li key={item.id}>
                                            <Link className="dropdown-item" to={"/country/"+item.id}>{item.name}</Link>

                                    </li>
                                    })}
                                    </ul>
                            </li>
                            { localStorage.getItem('user_id') == null ?
                            <>
                            <li className="nav-item">
                                    <Link to="/signUp" className="nav-link color-pink-hover" >Sign Up</Link>
                            </li>
                            <li className="nav-item">
                                    <Link to="/signIn" className="nav-link color-pink-hover" >Sign In</Link>
                                </li>
                                </> :
                                     <li className="nav-item">
                                     <a type="button" href="javascript:void(0)" onClick={handleSignOut} className="nav-link color-pink-hover" >Sign Out</a>
                                 </li>
                                }

                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        </div>
    )
}
    export default NavBar
