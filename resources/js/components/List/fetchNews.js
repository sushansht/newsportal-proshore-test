import { useEffect, useState } from 'react'
import axios from 'axios'

export default function fetchNews(query, pageNumber) {

  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(false)
  const [news, setNews] = useState([])
  const [hasMore, setHasMore] = useState(false)


  useEffect(() => {
    setNews([])
  }, [query])

  useEffect(() => {
    setLoading(true)
    setError(false)
    let cancel

    query.page = pageNumber;

    axios({
      method: 'GET',
      url: 'http://localhost:8000/api/v1/list',
      params:   query,

    //   params: { q: query, page: pageNumber, per_page : 10 , language : 'en' },
      cancelToken: new axios.CancelToken(c => cancel = c)
    }).then(res => {
        console.log(res)
      setNews(prevNews => {
        return [...new Set([...prevNews, ...res.data.articles])]
      })
      setHasMore(res.data.articles.length > 0)
      setLoading(false)
    }).catch(e => {
      if (axios.isCancel(e)) return
      setError(true)
    })
    return () => cancel()
  }, [query, pageNumber])

  return { loading, error, news, hasMore}
}
