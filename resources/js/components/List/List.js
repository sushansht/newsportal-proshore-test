import React from 'react'
import {  useState, setState, useRef, useCallback, useEffect } from "react";
import fetchNews from "./fetchNews"
import Detail from "./Detail"
import moment from 'moment'
import { useParams } from 'react-router';
import {useAuth} from '../Contexts/AuthContext';

function List(props) {
    const user = useAuth();
    const {categoryName} = useParams();
    const {sourceName} = useParams();
    const {countryName} = useParams();
    const [query, setQuery] = useState({});

    const [search, setSearch] = useState()

    useEffect(() => {
        const q = {
            'language' : 'en',
            'per_page' : 10,
            'category' : categoryName,
            'source' : sourceName,
            'country' : countryName
        };

       setQuery(q);
    }, [setQuery, categoryName , sourceName , countryName])


    const [pageNumber, setPageNumber] = useState(1)
    const [article, setArticle] = useState({});
    const [showDetail, setShowDetail] = useState(false);
    const closeDetail = () => setShowDetail(false);


    const {
        news,
        hasMore,
        loading,
        error
      } = fetchNews(query, pageNumber)



      const observer = useRef()
      const lastNewsElementRef = useCallback(node => {
        if (loading) return
        if (observer.current) observer.current.disconnect()
        observer.current = new IntersectionObserver(entries => {
          if (entries[0].isIntersecting && hasMore) {
            setPageNumber(prevPageNumber => prevPageNumber + 1)
          }
        })
        if (node) observer.current.observe(node)
      }, [loading, hasMore])

      function openDetail(article){
        setArticle(article);
        setShowDetail(true);
    }

   return (
         <div>
             <section className="section wb">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div className="page-wrapper">
                            <div className="blog-list clearfix">
                                <ul>
                            {news.map((item, index) => {
                                  if (news.length === index + 1) {
                                    return <li key={item.url} ref={lastNewsElementRef} className="list-group-item" style={{borderBlockWidth: "1"}}>
                                <div className="blog-box row">
                                    <div className="col-md-4">
                                        <div className="post-media">

                                                <img src={item.urlToImage} onClick={() => openDetail(item)} alt="" className="img-fluid" />
                                                <div className="hovereffect"></div>

                                        </div>
                                    </div>

                                    <div className="blog-meta big-meta col-md-8">
                                        <h4   onClick={() => openDetail(item)} >{item.title}</h4>
                                        <p>{item.description}</p>
                                        <small>{item.source.name}</small>
                                        <small>{moment(item.publishedAt).format('MMMM Do YYYY')}</small>
                                        <small>by {item.author}</small>
                                    </div>
                                </div>
                                    </li>
                                // <hr className="invis>

                                    } else {
                                        return  <li key={item.url} className="list-group-item" style={{borderBlockWidth: "1"}}>
                                         <div className="blog-box row">
                                    <div className="col-md-4">
                                        <div className="post-media">

                                                <img onClick={() => openDetail(item)} src={item.urlToImage} alt="" className="img-fluid" />
                                                <div className="hovereffect"></div>

                                        </div>
                                    </div>

                                    <div className="blog-meta big-meta col-md-8">
                                        <h4 onClick={() => openDetail(item)}>{item.title}</h4>
                                        <p>{item.description}</p>
                                        <small>{item.source.name}</small>
                                        <small>{moment(item.publishedAt).format('MMMM Do YYYY')}</small>
                                        <small>by {item.author}</small>
                                    </div>
                                </div>
                                    </li>
                                    }
                                    }
                                    )}
</ul>
                            </div>


                        </div>


                    </div>
                </div>
            </div>
        </section>

        <Detail show={showDetail} close={closeDetail} article={article}/>
        </div>
    )
}

export default List
