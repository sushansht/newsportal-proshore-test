import React, { useState } from 'react'
import { Modal } from "react-bootstrap";
import moment from 'moment'
import { useAuth } from '../Contexts/AuthContext'


function Detail({show , close, article}) {
    const { currentUser } = useAuth()
    const [error , setError] = useState()
    const [success , setSuccess] = useState()

    function handleLike(article){
       const data = {
            'user_id' :localStorage.getItem('user_id'),
            'news_url' : article.url,
            'like_state' : 1
        }
         const formData = new FormData();
        Object.keys(data).forEach(key => formData.append(key, data[key]));

        axios.post(
            "http://localhost:8000/api/v1/react", formData , { headers : {
                'Accept': 'application/json',
                'Authorization': localStorage.getItem('access_token')
            }}
          ).then(res => {
                 setSuccess('Thankyou for the reaction')
          }).catch(err => {
                setError('Something went wrong')
          })
    }

    function handleDisLike(article){
        const data = {
            'user_id' :localStorage.getItem('user_id'),
            'news_url' : article.url,
            'like_state' : 0
        }
         const formData = new FormData();
        Object.keys(data).forEach(key => formData.append(key, data[key]));

        axios.post(
            "http://localhost:8000/api/v1/react", formData , { headers : {
                'Accept': 'application/json',
                'Authorization': localStorage.getItem('access_token')
            }}
          ).then(res => {
            setSuccess('Thankyou for the reaction')
     }).catch(err => {
           setError('Something went wrong')
     })
    }

   return (
         <div>
                    <Modal show={show} onHide={close} animation={false} scrollable={true}  size="lg">
                    <Modal.Body>
                    <section class="section wb">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                    {error && <span className="error">{error}</span>}
                    {success && <span className="error">{success}</span>}
                        <div class="page-wrapper">

                            <div class="blog-title-area text-center">
                                <h3>{article.title}</h3>
                                <div class="blog-meta big-meta">
                                <small>{moment(article.publishedAt).format('MMMM Do YYYY')}</small>
                                    {/* <small>{article.source && article.source}</small> */}
                                    <small>by {article.author}</small>
                                </div>
                                <div class="topsocial">
                                <a href="#" type="button" onClick={() => handleLike(article)} data-toggle="tooltip" data-placement="bottom"><i class="fa fa-thumbs-up"></i> {article.like_count}</a>
                                <a href="#"  type="button" onClick={() => handleDisLike(article)}  data-toggle="tooltip" data-placement="bottom"><i class="fa fa-thumbs-down"></i> {article.dislike_count}</a>
                            </div>

                            </div>

                            <div class="single-post-media">
                                <img src={article.urlToImage} alt="" class="img-fluid" />
                            </div>

                            <div class="blog-content">
                                <div class="pp">
                                    <p>{article.content}</p>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </section>
    </Modal.Body>
                    </Modal>
        </div>
    )
}

export default Detail
