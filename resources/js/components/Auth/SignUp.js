import React, { useState } from 'react';
import { useForm } from "react-hook-form"
import { useHistory } from 'react-router-dom';
import { useAuth  } from '../Contexts/AuthContext'

function SignUp ()
{
const {register , handleSubmit, handleData} = useForm();
const [error , setError] = useState()
const history = useHistory();
const { signUp } = useAuth();

    const onSubmit = (data) => {
        const formData = new FormData();
        Object.keys(data).forEach(key => formData.append(key, data[key]));
        signUp(formData)
        .then(data =>
            history.push("/")
        )
        .catch( err =>{
            const signup_error = err.response.data.errors
            setError('Cannot Signup , Please verify inputs!')
        }
        )
    }
    return (
        <>
        <div className="row justify-content-center m-2">
         <div class="col-lg-6">
                <form class="form-wrapper" onSubmit={handleSubmit(onSubmit)} >
                <div className="row justify-content-center">
                        <h4>Sign Up</h4>
                        </div>
                        <div class="form-group">
                        {error && <span className="error">{error}</span>}
                                <input type="text" class="form-control" placeholder="Your name" name="name" {...register('name')} />
                                </div>
                                <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email address" name="email" {...register('email')} />
                                </div>
                                <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password"  name="password" {...register('password')}/>
                                </div>
                                <div className="row justify-content-center">
                                <button type="submit" class="btn btn-primary">Sign Up </button>
                                </div>
                    </form>
            </div>

            </div>
            {/* </div> */}
      </>
    );
  }

  export default SignUp
