import React, { useState } from 'react';
import { useForm } from "react-hook-form"
import axios, { AxiosResponse } from 'axios'
import { useHistory } from 'react-router-dom'
import { useAuth } from '../Contexts/AuthContext'

function SignIn ()
{
const {register , handleSubmit, handleData ,errors} = useForm();
const [ error , setError ] = useState()
const history = useHistory()
const { signIn, changeCurrentUser , currentUser } = useAuth()


    const onSubmit = (data) => {
        const formData = new FormData();
        Object.keys(data).forEach(key => formData.append(key, data[key]));

        signIn(formData)
              .then(response => {
                  changeCurrentUser(response.data.user)
                 localStorage.setItem("access_token" ,"Bearer "+response.data.access_token);
                 localStorage.setItem("user_id" ,response.data.user.id);
                 history.push('/')
              }).catch( err =>{
                setError('Cannot SignIn , Please verify inputs!')
            })

    }



    return (
      <>
     <div className="row justify-content-center m-2">
         <div className="col-lg-6">
                <form className="form-wrapper" onSubmit={handleSubmit(onSubmit)} >
                <div className="row justify-content-center">
                        <h4>Sign In</h4>
                        </div>
                        <div className="form-group">
                        {error && <span className="error">{error}</span>}
                                <input type="text" className="form-control" placeholder="Email address"{...register('email')} name="email"  />
                                </div>
                                <div className="form-group">
                                <input type="password" className="form-control" placeholder="Password"  {...register('password')}  name="password"/>
                                </div>
                                <div className="row justify-content-center">
                                <button type="submit" className="btn btn-primary">Sign In </button>
                                </div>
                    </form>
            </div>

            </div>
      </>
    );
  }

  export default SignIn
