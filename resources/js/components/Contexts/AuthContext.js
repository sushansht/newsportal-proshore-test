import React, {useContext, useState , useEffect} from 'react'

const AuthContext = React.createContext()

export function useAuth() {
    return useContext(AuthContext)
}

export function AuthProvider({children}) {
    const [currentUser , setCurrentUser] = useState({})
    const [loading, setLoading] = useState(true)

    function signUp(formData) {
        return axios.post(
            "http://localhost:8000/api/v1/signUp", formData , { headers : {
                'Accept': 'application/json'
            }}
          )
    }

    function signIn(formData) {
       return axios.post(
            "http://localhost:8000/api/v1/signIn", formData , { headers : {
                'Accept': 'application/json'
            }}
          )
    }

    function changeCurrentUser(user)
    {
        setCurrentUser(user)
    }

    function refreshToken(formData) {
        return axios.post(
             "http://localhost:8000/api/v1/token/refresh", formData , { headers : {
                 'Accept': 'application/json',
                 'Authorization': localStorage.getItem('access_token')
             }}
           )
     }

    function signOut() {
        return axios.get(
            "http://localhost:8000/api/v1/signOut" , { headers : {
                'Accept': 'application/json',
                'Authorization': localStorage.getItem('access_token')
            }}
          )
      }


      useEffect(() => {
        // const unsubscribe = auth.onAuthStateChanged(user => {
        //   setCurrentUser(user)
        //   setLoading(false)
        // })

        // return unsubscribe
      }, [])

    const value = {
        currentUser,
        signIn,
        signUp,
        signOut,
        refreshToken,
        changeCurrentUser
    }

    return (
        <AuthContext.Provider value={value}>
            { !!loading && children}
        </AuthContext.Provider>
    )
}
